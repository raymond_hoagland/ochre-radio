requirejs.config({
    paths: {
      'react': "/js/dependencies/react-min",
    },
});

require(['react'],
  function (React) {
    var SongInfo = React.createClass({
      getInitialState() {
        io.socket.get('/join/kllc', function() {
          console.log('joined kllc');
        });
        return {song_title: 'cans'};
      },
      componentDidMount() {
        io.socket.on('mess', function(data) {
          this.setState({song_title: data['metadata']});
        }.bind(this));
      },
      render() {
        var title = this.state.song_title;
        return React.createElement("p", null,  "Now playing ", title);
      }
  });

  React.render(React.createElement(SongInfo, {}), metadata);
});
