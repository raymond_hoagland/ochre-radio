var icy = require('icy')
var Speaker = require('speaker')
//var lame = require('lame')
var fs = require('fs')
var ffmpeg = require('fluent-ffmpeg')
//var Throttle = require('throttle')

var url =  'http://54.211.91.144/entercom-kufxfmaac-64?session-id=1565641999'
//'http://9513.live.streamtheworld.com:443/KLLCFMAAC_SC'

var speaker = new Speaker({
    channels: 2,          // 2 channels
    bitDepth: 16,         // 16-bit samples
    sampleRate: 22050     // 44,100 Hz sample rate
});

function record(url) {
  icy.get(url, function(res) {
    console.error(res.headers);

    res.on('metadata', function(metadata){
      var parsed = icy.parse(metadata);
      console.error(parsed);
      var stream = fs.createWriteStream('./me.mp3');
      var decoder = ffmpeg(res).audioBitrate('96k').format('mp3').audioFrequency(22050).on('end', function() {
        console.log('stream ended');
      }).on('error', function(err) {
        console.log('error encountered ' + err.message);
      }).pipe(stream, {end:true});
    })
  });
}

function playbaci(speaker){
    var stream = fs.createReadStream('./me.mp3');
    var decoder = ffmpeg(stream).audioBitrate('96k').format('s16le').audioFrequency(22050).pipe(speaker, {end:true});
}

function stream_radio(url, speaker) {
  icy.get(url, function(res) {
    console.error(res.headers);

    res.on('metadata', function(metadata){
      var parsed = icy.parse(metadata);
      console.error(parsed);
      var decoder = ffmpeg(res).audioBitrate('96k').format('s16le').audioFrequency(22050).on('end', function() {
        console.log('stream ended');
      }).on('error', function(err) {
        console.log('error encountered ' + err.message);
      }).pipe(speaker, {end:true});
    })
  });
}

//record(url);
//playback(speaker);
stream_radio(url, speaker);
