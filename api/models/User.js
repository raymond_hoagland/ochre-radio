/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    'username': {
      type: 'string',
      primaryKey: true,
      required: true
    },
    'nickname': {
      type: 'string',
      defaultsTo: 'anon'
    },
    'favorites': {
      type: 'array',
      defaultsTo: []
    },
    'active_stream_callsign': {
      type: 'string',
      defaultsTo: 'N/A'
    },

    set_nickname: function (options, cb) {
      User.findOne(options.username).exec(function (err, theUser) {
        if (err) return cb(err);
        if (!theUser) return cb(new Error('User not found.'));
        theUser.nickname = options.nickname;
        theUser.save(cb);
      });
    },

    add_favorite: function (options, cb) {
      User.findOne(options.username).exec(function (err, theUser) {
        if (err) return cb(err);
        if (!theUser) return cb(new Error('User not found.'));
        theUser.favorites.add(options.nickname);
        theUser.save(cb);
      });
    },

    get_favorites: function(options, cb) {
      User.findOne(options.username).exec(function (err, theUser) {
        if (err) return cb(err);
        if (!theUser) return cb(new Error('User not found.'));
        return cb(theUser.favorites);
      });
    },

    open_stream: function (options, cb) {
      options.user.active_stream_callsign = options.callsign;
      options.user.save(cb);
    }

  },

};
