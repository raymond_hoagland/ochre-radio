/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /**
   * `UserController.create()`
   */
  create: function (req, res) {
		User.create({
			username: req.params['username'],
			nickname: 'annn',
		}).exec(function (err, created) {
			if (err) {
				console.log(err);
			}
			else {
				console.log('created user with username ' + created.username);
			}
		});
    return res.json({
      todo: 'create() is not implemented yet!'
    });
  },


  /**
   * `UserController.destroy()`
   */
  destroy: function (req, res) {
		User.findOne({username: req.params['username']}).exec(function (err, retrieved) {
			if (err) {
				console.log('error encountered');
			}
			else if (retrieved == undefined) console.log('no user found');
			else {
				User.destroy({username: req.params['username']}).exec(function (err) {
					if (err) {
						console.log(err);
					}
					else {
						console.log('deleted user with username ' + req.params['username']);
					}
				});
			}
		});
    return res.json({
      todo: 'destroy() is not implemented yet!'
    });
  },


  /**
   * `UserController.authenticate()`
   */
  authenticate: function (req, res) {
		return res.json({
      todo: 'authenticate() is not implemented yet!'
    });
		/*
		return res.view('home', {
			'id': 1234
		});
		*/
  },


  /**
   * `UserController.add_favorite()`
   */
  add_favorite: function (req, res) {
    return res.json({
      todo: 'add_favorite() is not implemented yet!'
    });
  },

	/**
   * `UserController.set_stream()`
   */
  set_stream: function (req, res) {
		User.findOne({username: req.params['username']}).exec(function (err, retrieved){
			if (err) console.log('err countered');
			else if (retrieved == undefined) console.log('no user found');
			else {
				retrieved.open_stream({user: retrieved, callsign: req.params['callsign']}, function() {
					sails.controllers.radio.stream(req, res);
					/*
					let socket = io.connect();
					socket.get('/todo', function (message) {
						console.log('watching!');
					})
					*/
				});
			}
		});
    return res
  },

};
