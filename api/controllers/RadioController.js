/**
 * RadioController
 *
 * @description :: Server-side logic for managing radios
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
 let icy = require('icy')
 let Speaker = require('speaker')
 let ffmpeg = require('fluent-ffmpeg')
 let Throttle = require('throttle')

 // let url =  'http://54.158.40.1/entercom-kufxfmaac-64?session-id=1202887522'
 let url = 'http://54.211.91.144/entercom-kufxfmaac-64?session-id=1565641999'
 const speaker = new Speaker({
     channels: 2,          // 2 channels
     bitDepth: 16,         // 16-bit samples
     sampleRate: 22050     // 44,100 Hz sample rate
 });

 const throttle = new Throttle(96000);

module.exports = {

	/**
   * `RadioController.create()`
   */
	create: function (req, res) {
		Stream.create({
			callsign: req.params['callsign'],
			url: req.params['url']
		}).exec(function (err, created) {
			if (err) {
				console.log(err);
			}
			else {
				console.log('created stream with callsign ' + created.callsign);
			}
		});

    return res.view('home', {
			'id': 1234
    });
  },

	/**
   * `RadioController.destroy()`
   */
	destroy: function (req, res) {
		Stream.destroy({
			callsign: req.params['callsign'],
		}).exec(function (err) {
			if (err) {
				console.log(err);
			}
			else {
				console.log('removed stream with callsign ' + req.params['callsign']);
			}
		});

    return res.view('home', {
			'id': 1234
    });
  },

	/**
   * `RadioController.join()`
   */
  join: function(req, res) {
		sails.sockets.join(req.socket, 'kllc');
		//sails.sockets.broadcast('kllc', 'mess', {'omg': 'apples'});

		return res.view('home', {
			'username': req.params['username'],
			'callsign': req.params['callsign']
		});
	},

  /**
   * `RadioController.stream()`
   */
  stream: function (req, res) {
		//sails.sockets.emit('mess', 'mess');
		Stream.findOne({callsign: req.params['callsign']}).exec(function (err, retrieved) {
			if (err) {
				console.log('error encountered');
			}
			else if (retrieved == undefined) console.log('no matching stream found');
			else {

					let mk = icy.get(url, function(stream) {
				    console.error(stream.headers);

						let decoder = ffmpeg(stream).audioBitrate('96k').format('s16le').audioFrequency(22050).on('end', function() {
							console.log('stream ended');
							decoder.unpipe(th);
						}).on('error', function(err) {
							console.log('error encountered ' + err.message);
						}).pipe(throttle);//.pipe(speaker, {end:true});

				    stream.on('metadata', function(metadata){
				      let parsed = icy.parse(metadata);
							console.error(parsed);
							sails.sockets.broadcast('kllc', 'mess', {'metadata': parsed['StreamTitle']});
				    });

						throttle.pipe(speaker);
				  });
				}
		});

		return res.view('stream', {
			'username': req.params['username'],
			'callsign': req.params['callsign']
		});
  },


  /**
   * `RadioController.record()`
   */
  record: function (req, res) {
    return res.json({
      todo: 'record() is not implemented yet!'
    });
  },


  /**
   * `RadioController.load()`
   */
  load: function (req, res) {
    return res.json({
      todo: 'load() is not implemented yet!'
    });
  }
};
